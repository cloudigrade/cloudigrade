# Generated by Django 2.0.7 on 2018-08-01 20:49

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0014_make_awsinstanceevent_subnet_optional'),
    ]

    operations = [
        migrations.AddField(
            model_name='machineimage',
            name='name',
            field=models.CharField(blank=True, max_length=256, null=True),
        ),
    ]
