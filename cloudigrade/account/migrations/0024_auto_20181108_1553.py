# Generated by Django 2.1.1 on 2018-11-08 15:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0023_add_new_event_type'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='name',
            field=models.CharField(db_index=True, max_length=256),
        ),
    ]
