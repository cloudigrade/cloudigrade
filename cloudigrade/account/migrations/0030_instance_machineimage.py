# Generated by Django 2.1.2 on 2019-01-23 16:18

from django.db import migrations, models
import django.db.models.deletion


def populate_machineimage(apps, schema_editor):
    '''
    Populates the machine image foreign key on instance, with values from instanceevents
    '''
    Instances = apps.get_model('account', 'Instance')
    InstanceEvents = apps.get_model('account', 'InstanceEvent')

    for instance in Instances.objects.all():
        instanceevent = InstanceEvents.objects.filter(
            instance=instance,
            machineimage__isnull=False,
        ).order_by('-occurred_at').first()

        if instanceevent:
            machineimage = instanceevent.machineimage

            instance.machineimage=machineimage
            instance.save()

class Migration(migrations.Migration):

    dependencies = [
        ('account', '0029_merge_20190122_1627'),
    ]

    operations = [
        migrations.AddField(
            model_name='instance',
            name='machineimage',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='account.MachineImage'),
        ),
        migrations.RunPython(populate_machineimage)
    ]
