# Generated by Django 2.1.2 on 2018-12-17 18:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0024_auto_20181108_1553'),
    ]

    operations = [
        migrations.AlterField(
            model_name='awsmachineimage',
            name='owner_aws_account_id',
            field=models.DecimalField(decimal_places=0, max_digits=12, null=True),
        ),
        migrations.AlterField(
            model_name='awsmachineimage',
            name='platform',
            field=models.CharField(choices=[('none', 'None'), ('windows', 'Windows')], default='none', max_length=7, null=True),
        ),
        migrations.AlterField(
            model_name='machineimage',
            name='status',
            field=models.CharField(choices=[('pending', 'Pending Inspection'), ('preparing', 'Preparing for Inspection'), ('inspecting', 'Being Inspected'), ('inspected', 'Inspected'), ('error', 'Error'), ('unavailable', 'Unavailable for Inspection')], default='pending', max_length=10),
        ),
    ]
