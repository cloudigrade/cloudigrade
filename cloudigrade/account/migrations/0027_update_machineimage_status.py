# Generated by Django 2.1.2 on 2018-11-14 17:36
from decimal import Decimal

from django.db import migrations
from django.db.models import Q


def update_images_as_inspected(apps, schema_editor):
    """
    Update inspection status for marketplace and cloud access images.

    Notes:
        We duplicate several things as hard-coded values here instead of using
        references to the original variables in the application code because
        migrations like this need to be able to run successfully in the future
        when those variables may no longer exist or may have changed.
    """
    AwsMachineImage = apps.get_model('account', 'AwsMachineImage')
    AwsMachineImage.objects.filter(
        Q(name__contains='-Access2') | Q(name__contains='-hourly2'),
        owner_aws_account_id__in=[
            Decimal('841258680906'),  # china
            Decimal('219670896067'),  # govcloud
            Decimal('309956199498'),  # all others
        ],
    ).exclude(
        status='inspected'
    ).update(
        status='inspected'
    )


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0026_machineimage_20181219_1842'),
    ]

    operations = [
        migrations.RunPython(update_images_as_inspected),
    ]
