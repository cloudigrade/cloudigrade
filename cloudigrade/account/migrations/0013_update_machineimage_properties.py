# Generated by Django 2.0.7 on 2018-07-30 05:41

from django.db import migrations, models


def migrate_tags_to_properties(apps, schema_editor):
    """Convert ImageTags into AwsMachineImage properties."""
    AwsMachineImage = apps.get_model('account', 'AwsMachineImage')
    ImageTag = apps.get_model('account', 'ImageTag')

    rhel_tag = ImageTag.objects.get(description='rhel')
    openshift_tag = ImageTag.objects.get(description='openshift')
    windows_tag = ImageTag.objects.get(description='windows')

    for image in AwsMachineImage.objects.filter(tags__id=rhel_tag.id):
        image.rhel_detected = True
        image.save()

    for image in AwsMachineImage.objects.filter(tags__id=openshift_tag.id):
        image.openshift_detected = True
        image.save()

    for image in AwsMachineImage.objects.filter(tags__id=windows_tag.id):
        image.platform = image.WINDOWS
        image.save()


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0012_instanceevent_machineimage_fkey'),
    ]

    operations = [
        migrations.AddField(
            model_name='awsmachineimage',
            name='platform',
            field=models.CharField(choices=[('none', 'None'), ('windows', 'Windows')], default='none', max_length=7),
        ),
        migrations.AddField(
            model_name='machineimage',
            name='openshift_challenged',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='machineimage',
            name='openshift_detected',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='machineimage',
            name='rhel_challenged',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='machineimage',
            name='rhel_detected',
            field=models.BooleanField(default=False),
        ),
        migrations.RunPython(migrate_tags_to_properties),
        migrations.RemoveField(
            model_name='machineimage',
            name='tags',
        ),
        migrations.DeleteModel(
            name='ImageTag',
        ),
    ]
